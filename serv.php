<?php

	require_once('HttpServer.class.php');

	use \ServerCore\HttpServer;

	set_time_limit(0);

	$ip_address = "0.0.0.0";
	$port_number = 80;
	$root = count($argv) > 1 ? strlen($path = realpath(dirname(__FILE__).DIRECTORY_SEPARATOR.$argv[1])) === 0 ? $argv[1] : $path : dirname(__FILE__);

	$hs = new HttpServer($ip_address, $port_number, $root);
	$hs->create();
	$hs->loop(
		array(function($uri, $handler){
			printf("FIRST HANDLER $uri (%d)\n", is_file($uri));
			if(is_file($uri))
				return $handler->file();
		}, function($uri, $handler){
			printf("SECOND HANDLER\n");
			return $handler->file("\config\dispatcher.php");
		})
	);

	$hs->close();