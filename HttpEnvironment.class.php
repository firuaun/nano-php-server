<?php

namespace ServerCore;

require_once('HttpRequest.class.php');
require_once('HttpRequestParser.class.php');
require_once('HttpServer.class.php');

abstract class HttpEnvironment {

	public static function Prepare(HttpRequest $request) {
		$headers = $request->headers();
		$_REQUEST += $request->params();
		printf("[HttpEnvironment][Prepare] \$params: %d\n",count($request->params()));
		if($request->isGet()) {
			$_GET = $request->params();
		}
		else {
			$_POST = $request->params();
		}
		printf("[HttpEnvironment][Prepare] \$_REQUEST: %d\n",count($_REQUEST));
		printf("[HttpEnvironment][Prepare] \$_POST: %d\n",count($_POST));
		printf("[HttpEnvironment][Prepare] \$_GET: %d\n",count($_GET));
		$_SERVER =  $headers;
		//ini_set('open_basedir', HttpServer::$server["ROOT_DIR"]);
	}

	public static function CleanUp() {
		//ini_set('open_basedir', '');
		chdir(HttpServer::$server["ROOT_DIR"]);
	}

	public static function InnerRedirectPath($request, $path) {
		$request->head("REAL_REQUEST_PATH", HttpRequestParser::translate_path_to_real($path));
		printf("[HttpEnvironment][inner_redirect_path] changed path to: %s (given path: %s)\n", $request->head("REAL_REQUEST_PATH"), $path);
		chdir(dirname($request->head("REAL_REQUEST_PATH")));
	}

}