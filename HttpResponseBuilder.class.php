<?php

namespace ServerCore;

require_once('HttpConstants.config.php');
require_once('HttpResponse.class.php');
require_once('HttpStatusCode.enum.php');

use HttpEnums\HttpReasonPhase;

class HttpResponseBuilder {

	private $response;

	function __construct() {
		$this->response = new HttpResponse();
		$this->response->date = date(DATE_RFC850);
	}

	function SetServer($server) {
		$this->response->server = $server;
		return $this;
	}

	function SetContent($content) {
		$this->response->content = $content;
		$this->response->content_length = strlen($content);
		return $this;
	}

	function SetContentType($type) {
		$this->response->content_type = $type;
		return $this;
	}

	function SetStatusCode($status) {
		$this->response->status_code = $status;
		return $this;
	}

	function Build() {
		return sprintf(
					"%s %d %s\r\n".
					"Server: %s\r\n".
					"Date: %s\r\n". 
					"Content-Length: %d\r\n".
					"Content-Type: %s\r\n\r\n".
					"%s",
					HTTP_VER, $this->response->status_code, HttpReasonPhase::GetReasonPhaseByHttpStatusCode($this->response->status_code),
					$this->response->server,
					$this->response->date,
					$this->response->content_length,
					$this->response->content_type,
					$this->response->content
				);
	}

	function Get() {
		return $response;
	}
}