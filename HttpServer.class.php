<?php
namespace ServerCore;

require_once('ContentType.enum.php');
require_once('HttpEnvironment.class.php');
require_once('HttpRequest.class.php');
require_once('HttpResponseBuilder.class.php');
require_once('HttpRequestHandler.class.php');
require_once('HttpStatusCode.enum.php');
require_once('Promise.class.php');

define("SERVER_NAME", "NanoPhpServ");

use \HttpEnums\ContentType;
use \HttpEnums\HttpStatusCode;
use \Utils\Promise;

	class HttpServer {

		private $ip_address;
		private $port_number;
		private $socket;

		static public $server = array();

		function __construct($__ip_address, $__port = 80, $base) {
			printf("[HttpServer][__construct] invoked, basepath: %s\n", $base);
			$this->ip_address = $__ip_address;
			$this->port_number = $__port;
			self::$server["ROOT_DIR"] = $base;
			chdir($base);
			set_error_handler(array($this,"fatal_error_handler"));
		}
		function create() {
			$this->socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
			Promise::that(socket_bind($this->socket, $this->ip_address, $this->port_number))
				->when(false)
				->then(function($r){
					printf("[HttpServer][create][SOCKET_BIND][PROMISE][THEN] false\n");
				})
				->otherwise(function($r){
					Promise::that(socket_listen($this->socket, 5))->when(false)->then(function(){
						printf("[HttpServer][create][SOCKET_LISTEN][PROMISE][THEN] false\n");
					})->fullfill();
				})
				->fullfill();
		}
		function accept() {
			return socket_accept($this->socket);
		}
		function close() {
			socket_close($this->socket);
		}
		function loop($handlers = null) {
			$handler = new HttpRequestHandler($handlers);
			do {
				$accepted_socket = $this->accept();
				$request = HttpRequest::fetch($accepted_socket);
				if($request == null)
					continue;
				printf("[HttpServer][loop] msg: %s\n",$request->body());
				$response_builder = (new HttpResponseBuilder())->SetServer(SERVER_NAME);
				try {
					HttpEnvironment::Prepare($request, $this);
					$handler->handle($request, $response_builder->SetStatusCode(HttpStatusCode::OK));
				}
				catch(Exception $e) {
					//TODO: catch (fatal) errors as exceptions
					$response_builder->SetStatusCode(HttpStatusCode::INTERNAL_ERROR)->SetContentType(ContentType::PLAIN)->SetContent("500 Internal Server Error\n".$e->getMessage());
				}
				finally {	
					socket_write($accepted_socket, $response_builder->Build());
					socket_close($accepted_socket);
					HttpEnvironment::CleanUp();
				}
			} while(true);
		}

		function fatal_error_handler($errno, $errstr, $errfile, $errline, $errcontext) {
			throw new \Exception(sprintf("FATAL ERROR CAUGHT: [%d] %s in %s on %d.\n",$errno, $errstr, $errfile, $errline));
		}
	}
