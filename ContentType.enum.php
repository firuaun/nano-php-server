<?php

namespace HttpEnums;

abstract class ContentType {
	const OCTET_APPLICATION = "";
	const HTML = "text/html; charset=utf-8";
	const PLAIN = "text/plain; charset=utf-8";
	const IMAGE = "image/%s";
}