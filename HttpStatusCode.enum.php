<?php

namespace HttpEnums;

abstract class HttpStatusCode {
	const OK = 200;
	const NOT_FOUND = 404;
	const INTERNAL_ERROR = 500;
}

abstract class HttpReasonPhase {
	private static $status_reason_array = array(
		100 => "Informational",
		200 => "OK",
		300 => "Redirection",
		400 => "Client Error",
		404 => "Not Found",
		500 => "Internal Server Error"
	);
	public static function GetReasonPhaseByHttpStatusCode($status) {
		if(array_key_exists($status, self::$status_reason_array)) {
			return self::$status_reason_array[$status];
		}
		return "Unknown";
	}
}
