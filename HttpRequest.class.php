<?php

namespace ServerCore;

require_once('HttpRequestParser.class.php');

class HttpRequest {

	private $headers; 
	private $body;

	function isPost() {
		return $this->headers["METHOD"] == "POST";
	}

	function isGet() {
		return $this->headers["METHOD"] == "GET";
	}

	function __construct($headers, $body) {
		$this->headers = $headers;
		$this->body = $body;
	}

	public function head($key, $value = null) {
		if($value != null)
			$this->headers[$key] = $value;
		return $this->headers[$key];
	}

	public function headers() {
		return $this->headers;
	}

	public function body() {
		return $this->body;
	}

	public function params() {
		return $this->headers["PARAMS"];
	}

	public static function fetch($socket) {
		$buffers = HttpRequestParser::http_socket_header_read($socket);
		if($buffers == null)
			return null;
		$headers = HttpRequestParser::http_header_parse($buffers[HttpRequestParser::BUFFERS_HEADER_INDEX]);
		$body = HttpRequestParser::http_socket_body_read($socket, $headers, $buffers[HttpRequestParser::BUFFERS_PUT_BACK_INDEX]);
		$remote_ip = "0.0.0.0";
		$remote_port = 0;
		socket_getpeername($socket, $remote_ip, $remote_port);
		$headers["REMOTE_IP"] = $remote_ip;
		$headers["REMOTE_PORT"] = $remote_port;
		printf("[HttpRequest][fetch] ip: %s port: %d\n",$remote_ip, $remote_port);
		return new HttpRequest($headers, $body);
	}
}


