<?php

namespace ServerCore;

require_once('HttpConstants.config.php');
require_once('HttpServer.class.php');

define("READ_CHUNK_SIZE", 1024);

class HttpRequestParser {
	const BUFFERS_HEADER_INDEX = 0;
	const BUFFERS_PUT_BACK_INDEX = 1;

	public static function http_socket_header_read($socket) {
		$header = "";
		$put_back = "";
		while(false !== ($buffer = socket_read($socket, READ_CHUNK_SIZE)) && strlen($buffer) !== 0){
			printf("[HttpRequestParser][http_socket_header_read] buffer length: %d.\n", strlen($buffer));
			if(($pos = strpos($buffer,"\r\n\r\n")) !== false) {
				$header .= substr($buffer,0,$pos);
				$put_back .= substr($buffer,$pos+4);
				printf("[HttpRequestParser][http_socket_header_read] put back: %s.\n", $put_back);
				break;
			}
			$header .= $buffer;
		}
		if(strlen($header) == 0)
			return null;
		return array($header, $put_back);
	}

	public static function translate_path_to_real($path) {
		return HttpServer::$server["ROOT_DIR"].self::translate_path_to_virtual($path);
	}

	public static function translate_path_to_virtual($path) {
		return str_replace("/", DIRECTORY_SEPARATOR, $path);
	}

	public static function http_header_parse($raw_header) {
		$parsed_header = array();
		$first_line_end_pos = strpos($raw_header,"\r\n");
		$first_line = explode(" ", substr($raw_header,0,$first_line_end_pos));
		$url_and_params = self::parse_url($first_line[1]);
		$parsed_header["METHOD"] = $first_line[0];
		$parsed_header["REQUEST_URI"] = $url_and_params["URI"];
		$parsed_header["REQUEST_PATH"] = $first_line[1];
		$parsed_header["PARAMS"] = $url_and_params["PARAMS"];
		$parsed_header["REAL_REQUEST_PATH"] = self::translate_path_to_real($url_and_params["URI"]);
		$parsed_header["BASE_REQUEST_PATH"] = self::translate_path_to_virtual($url_and_params["URI"]);
		$parsed_header["HTTP_VERSION"] = $first_line[2];
		$raw_pairs = substr($raw_header, $first_line_end_pos + 2);
		foreach(explode("\r\n", $raw_pairs) as $line) {
			$pair = explode(":", $line);
			$parsed_header[trim($pair[0])] = trim($pair[1]);
		}
		return $parsed_header;
	}

	public static function http_socket_body_read($socket, &$parsed_header, $buffer) {
		if(array_key_exists("Content-Length", $parsed_header)) {	
			$content_size = (int) $parsed_header["Content-Length"];
			$buffer .= socket_read($socket, $content_size - strlen($buffer));
			if($parsed_header["METHOD"] == "POST") {
				$parsed_header["PARAMS"] += self::parse_params($buffer);
			}
		}
		return $buffer;
	}

	public static function parse_get_params($url) {
		return ($get_start_pos = strpos($url, "?")) ? self::parse_params(substr($url, $get_start_pos+1)) : array();
	}

	public static function parse_params($raw_params) {
		$params = array();
		$raw_pairs = explode("&", $raw_params);
		foreach ($raw_pairs as $value) {
			$pair = explode("=", $value);
			$params[$pair[0]] = urldecode($pair[1]);
			printf("[HttpRequestParser][parse_params] key: %s, value: %s\n", $pair[0], $params[$pair[0]]);
		}
		return $params;
	}

	public static function clean_url($url) {
		if($get_end_pos = strpos($url, "?")) {
			return substr($url, 0, $get_end_pos);
		}
		return $url;
	}

	public static function parse_url($url) {
		return array(
			"URI" => self::clean_url($url),
			"PARAMS" => self::parse_get_params($url)
		);
	}
}