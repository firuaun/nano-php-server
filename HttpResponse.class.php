<?php

namespace ServerCore;

class HttpResponse {
		/*
		*	Required headers:
		*		- Date: date(DATE_RFC850)
		*		- Content-Length
		*		- Content-Type (default application/octet-stream)
		*		- Server: custom name
		*/

		public $server;
		public $date;
		public $content_length;
		public $content_type;
		public $content;
		public $status_code;
	}