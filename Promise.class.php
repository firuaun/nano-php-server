<?php

namespace Utils;

class Promise {

	private $fail_handle;
	private $success_handle;
	private $promised;

	static function that($obj) {
		return new Promise($obj);
	}

	function __construct($__promised) {
		$this->promised = $__promised;
		$this->success_handle = function(){};
		$this->fail_handle = function(){};
		$this->condition = true;
	}

	private function fail($result) {
		$fail = $this->fail_handle;
		return $fail($result,$this->promised);
	}

	private function success($result) {
		$success = $this->success_handle;
		return $success($result,$this->promised);
	}

	function when($condition) {
		$this->condition = $condition;
		return $this;
	}

	function then($success) {
		$this->success_handle = $success;
		return $this;
	}

	function otherwise($fail) {
		$this->fail_handle = $fail;
		return $this;
	}

	function fullfill() {
		$result = $this->promised;
		if(is_object($this->promised) && get_class($this->promised) === "Closure") {
			$result = $this->promised();
		}
		return $result === $this->condition ? $this->success($result) : $this->fail($result);
	}
}