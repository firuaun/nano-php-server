<?php

namespace ServerCore;

require_once('ContentType.enum.php');
require_once('HttpEnvironment.class.php');
require_once('HttpStatusCode.enum.php');

use \HttpEnums\ContentType;
use \HttpEnums\HttpStatusCode;

static $ext_to_type = array(
		"jpg" => "image/jpeg",
		"jpeg" => "image/jpeg",
		"gif" => "image/gif",
		"png" => "image/png",
		"jpg" => "image/jpeg",
		"bmp" => "image/bmp",
		"ico" => "image/ico",
		"js" => "text/javascript",
		"css" => "text/css",
		"xml" => "text/xml"
);

function get_file_type($extension) {
	global $ext_to_type;
	if(array_key_exists($extension, $ext_to_type)) {
		return $ext_to_type[$extension];
	}
	return false;
}

class HttpRequestHandler {

	const INDEX_FILE_NAME = "index";

	private $handlers;
	private $request;
	private $response;

	public function __construct($handlers = null) {
		$this->handlers = $handlers;
	}

	public function handle(&$request, $response) {
		$path = $request->head("REAL_REQUEST_PATH");
		$this->request = $request;
		$this->response = $response;
		printf("[HttpRequestHandler][handle] path: %s\n", $path);
		if($this->handlers != null) {
			foreach($this->handlers as $hook) {
				if(($result = $hook($path, $this)))
					return $result;
			}
		}
		if(is_file($path))
			return $this->handleFile();
		else if(is_dir($path))
			return $this->handleDir();
		else
			return $this->handle404();
	}

	private function handleFile() {
		$path = $this->request->head("REAL_REQUEST_PATH");
		$ext = pathinfo($path, PATHINFO_EXTENSION);
		printf("[HttpRequestHandler][handleFile] path: %s, ext: %s\n", $path, $ext);
		if(in_array($ext, array("html","htm","php"))) {
			return $this->html($path);
		}
		else if(($file_type = get_file_type($ext)) !== false) {
			return $this->response->SetContentType($file_type)->SetContent(file_get_contents($path));
		}
		else {
			return $this->response->SetContentType(ContentType::PLAIN)->SetContent(file_get_contents($path));
		}
	}

	public function handle404() {
		return $this->response->SetStatusCode(HttpStatusCode::NOT_FOUND)->SetContentType(ContentType::PLAIN)->SetContent("404 Not Found");
	}

	private function handleDir() {
		$path = $this->request->head("REAL_REQUEST_PATH");
		$length = strlen(self::INDEX_FILE_NAME);
		printf("[HttpRequestHandler][handleDir] path: %s\n", $path);
		if($od = opendir($path)) {
			while(($file = readdir($od)) !== false) {
				$file_path = $path.DIRECTORY_SEPARATOR.$file;
				if(is_file($file_path) and substr(basename($file_path),0,$length) === self::INDEX_FILE_NAME) {
					return $this->html($file_path);
				}
			}
		}
		return $this->handle404();
	}

	private function html($file) {
		ob_start();
			include $file;
		return $this->response->SetContentType(ContentType::HTML)->SetContent(ob_get_clean());
	}

	public function file($file = null) {
		if($file != null) {
			HttpEnvironment::InnerRedirectPath($this->request, $file);
		}
		return $this->handleFile();
	}

	public function dir($dir) {
		return $this->handleDir();
	}

}